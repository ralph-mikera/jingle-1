# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'webscraper'
project = "Python webscraper"
project_no_spaces = project.replace(' ', '')
version = '0.1'
description = 'It scrapes a particular website'
authors = ['TENTEE GLOBAL']
authors_string = ', '.join(authors)
emails = ['franck.djouonang@tenteeglobal.com']
license = 'MIT'
copyright = '2018 ' + authors_string
url = 'http://example.com/'
