# How to run the webscrapper for the jingle project

### OS constraints:
- Ubuntu 16.04
- Python 3.6.0 and PIP
	- `sudo apt-get install python3`
	- `sudo apt-get install python3-setuptools`
	- `sudo apt-get install python3-pkg-resources`
- Google chrome 69.0.3497.92-1
	- `sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add`
	- `sudo echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list`
	- `sudo apt-get -y update`
	- `sudo apt-get -y install google-chrome-stable`
- chromedriver 2.41
	- `wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip`
	- `unzip chromedriver_linux64.zip`
	- `sudo mv chromedriver /usr/bin/chromedriver`
	- `sudo chown root:root /usr/bin/chromedriver`
	- `sudo chmod +x /usr/bin/chromedrive`
- virtualenv 15.0.1 
	- `sudo apt install virtualenv`

## Create a directory: it will be the virtualenv for this project
`mkdir webscrapper_env`

## cd to the directory
`cd webscrapper_env`

## create the virtualenv
`virtualenv -p python3 .`

## clone this repository
`git clone https://tenteeglobal@bitbucket.org/tenteeglobal/jingle.git`

## cd to the cloned repo
`cd jingle`

## Install the scrapper app
`pip3 install -e .`

## Install the app's requirements
`pip3 install -r requirements.txt`

## activate the virtualenv 
`source ../bin/activate`

## Scrape the target website
`webscraper_cli --url https://geizhals.at/?hlist`
#### The results will be created in the file webscrapper_env/jingle/shops_and_products with all the shops in a JSON format !


